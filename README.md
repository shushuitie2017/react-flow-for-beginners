# React-flow-for-Beginners

# 主要技术
html，css，js，react，hooks，typescript   
# UI库
MATERIAL-UI   
https://material-ui.com/
# 内容概要：
通过react 搭建前端页面，拖拽流程图节点自由组合。   
# flow库地址
https://reactflow.dev/examples/

# 如何加入我们
- 新手上路阶段，建议熟悉以下内容
- HTML（超文本标记语言） 预估0基础需要4小时掌握
- https://developer.mozilla.org/zh-CN/docs/Web/HTML
- CSS（层叠样式表）预估0基础需要4小时掌握
- https://developer.mozilla.org/zh-CN/docs/Web/CSS
- JavaScript ( JS ) 预估0基础需要8小时掌握
- https://developer.mozilla.org/zh-CN/docs/Web/JavaScript
# 进阶阶段
- 入门教程: 认识 React
- https://zh-hans.reactjs.org/tutorial/tutorial.html
- 《TypeScript Deep Dive》 的中文翻译版
- https://jkchao.github.io/typescript-book-chinese/
# 实战练习
- 使用 React Hooks + TypeSscript 对 react-dnd 用法重新梳理
- https://juejin.cn/post/6844903933463265293
- 然后把这个实战练习内容，引入第三方的流程库
- https://github.com/wbkd/react-flow

备注：项目环境搭建 npx create-react-app react-ts-app --template typescript

# 恭喜您，完成以上内容就可以开始干活了。
